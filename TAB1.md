# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the GcpCompute System. The API that was used to build the adapter for GcpCompute is usually available in the report directory of this adapter. The adapter utilizes the GcpCompute API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Google Cloud Platform Compute adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Google Cloud Platform. With this adapter you have the ability to perform operations such as:

- Configure and Manage Compute Resources in the Google Cloud Platform. 

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
