# Google Cloud Platform Compute

Vendor: Google
Homepage: https://cloud.google.com/

Product: Cloud Platform
Product https://cloud.google.com/products/compute

## Introduction
We classify Google Cloud Platform into the Cloud domain as it provide a solution for Allocating and Managing Resources in the Google Cloud. We also classify it into the Inventory domain because it contains an inventory of Google Cloud Compute.

"Google Cloud Platform provides secure and customizable compute services." 

The Google Cloud Platform Compute adapter can be integrated to the Itential Device Broker which will allow your Google Cloud Compute Resources to be managed within the Itential Configuration Manager Application.

## Why Integrate
The Google Cloud Platform Compute adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Google Cloud Platform. With this adapter you have the ability to perform operations such as:

- Configure and Manage Compute Resources in the Google Cloud Platform. 

## Additional Product Documentation
The [API documents for Google Cloud Platform Compute](https://console.cloud.google.com/marketplace/browse?filter=solution-type:saas)
Another [API documents for Google Cloud Platform Compute](https://cloud.google.com/vision/docs/reference/rest/?apix=true)
