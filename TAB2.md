# Google Cloud Platform Compute

## Table of Contents 

  - [Specific Adapter Information](#specific-adapter-information) 
    - [Authentication](#authentication) 
    - [Sample Properties](#sample-properties) 
    - [Swagger](#swagger) 
  - [Generic Adapter Information](#generic-adapter-information) 

## Specific Adapter Information
### Authentication

This document will go through the steps for authenticating the Google Cloud Platform Compute adapter with Google Token Security. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>.

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

#### Google Token Authentication
The Google Cloud Platform Compute adapter authenticates with a Google token. 

STEPS
1. Ensure you have access to a Google Cloud Platform Compute server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field

##### Using key in service config (Needed for Cloud Instance)
```json
"authentication": {
  "auth_method": "no_authentication",
  "configureGtoken": {
    "email": "sample@email.com",
    "key": "-----BEGIN PRIVATE KEY-----\nMIIEv********--\n",
    "scope": "https://www.********",
    "eagerRefreshThresholdMillis": 300000,
    "project_id": "myprojectid"
  }
}
```

##### Using keyfile in service config 
```json
"authentication": {
  "auth_method": "no_authentication",
  "configureGtoken": {
    "email": "",
    "scope": "https://www.googleapis.com/auth/cloud-platform",
    "sub": "",
    "keyFile": "/opt/pronghorn/RENAME_TO_YOUR_KEYFILE.json",
    "key": "",
    "additionalClaims": "",
    "eagerRefreshThresholdMillis": 300000
  }
}
```

you can leave all of the other properties in the authentication section, they will not be used for Google Cloud Platform Compute Google token authentication.
4. Restart the adapter. If your properties were set correctly, the adapter should go online.

#### Troubleshooting
- Make sure you copied over the correct Googgle properties as these are used to retrieve the token.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.

### Sample Properties

Sample Properties can be used to help you configure the adapter in the Itential Automation Platform. You will need to update connectivity information such as the host, port, protocol and credentials.

```json
  "properties": {
    "host": "compute.googleapis.com",
    "port": 443,
    "choosepath": "",
    "base_path": "/compute",
    "version": "v1",
    "cache_location": "none",
    "encode_pathvars": true,
    "encode_queryvars": true,
    "save_metric": false,
    "stub": false,
    "protocol": "https",
    "authentication": {
      "auth_method": "no_authentication",
      "username": "username",
      "password": "password",
      "token": "token",
      "invalid_token_error": 401,
      "token_timeout": 3600000,
      "token_cache": "local",
      "auth_field": "header.headers.Authorization",
      "auth_field_format": "Bearer {token}",
      "auth_logging": false,
      "client_id": "",
      "client_secret": "",
      "grant_type": "",
      "sensitive": [],
      "sso": {
        "protocol": "",
        "host": "",
        "port": 0
      },
      "multiStepAuthCalls": [
        {
          "name": "",
          "requestFields": {},
          "responseFields": {},
          "successfullResponseCode": 200
        }
      ],
      "configureGtoken": {
        "email": "",
        "scope": "https://www.googleapis.com/auth/cloud-platform",
        "sub": "",
        "keyFile": "/opt/pronghorn/RENAME_TO_YOUR_KEYFILE.json",
        "key": "",
        "additionalClaims": "",
        "eagerRefreshThresholdMillis": 300000
      }
    },
    "healthcheck": {
      "type": "startup",
      "frequency": 600000,
      "query_object": {},
      "addlHeaders": {}
    },
    "throttle": {
      "throttle_enabled": false,
      "number_pronghorns": 1,
      "sync_async": "sync",
      "max_in_queue": 1000,
      "concurrent_max": 1,
      "expire_timeout": 0,
      "avg_runtime": 200,
      "priorities": [
        {
          "value": 0,
          "percent": 100
        }
      ]
    },
    "request": {
      "number_redirects": 0,
      "number_retries": 3,
      "limit_retry_error": [
        0
      ],
      "failover_codes": [],
      "attempt_timeout": 5000,
      "global_request": {
        "payload": {},
        "uriOptions": {},
        "addlHeaders": {},
        "authData": {}
      },
      "healthcheck_on_timeout": true,
      "return_raw": false,
      "archiving": false,
      "return_request": false
    },
    "proxy": {
      "enabled": false,
      "host": "",
      "port": 1,
      "protocol": "http",
      "username": "",
      "password": ""
    },
    "ssl": {
      "ecdhCurve": "",
      "enabled": false,
      "accept_invalid_cert": false,
      "ca_file": "",
      "key_file": "",
      "cert_file": "",
      "secure_protocol": "",
      "ciphers": ""
    },
    "mongo": {
      "host": "",
      "port": 0,
      "database": "",
      "username": "",
      "password": "",
      "replSet": "",
      "db_ssl": {
        "enabled": false,
        "accept_invalid_cert": false,
        "ca_file": "",
        "key_file": "",
        "cert_file": ""
      }
    },
    "devicebroker": {
      "enabled": true,
      "getDevice": [
        {
          "path": "/{project}/global/networks",
          "method": "GET",
          "query": {},
          "body": {},
          "headers": {},
          "handleFailure": "ignore",
          "requestFields": {
            "project": "{project}"
          },
          "responseDatakey": "items",
          "responseFields": {
            "name": "{name}",
            "ostype": "{kind}",
            "ostypePrefix": "VPC Network-",
            "ipaddress": "{id}",
            "port": "{selfLinkWithId}"
          }
        }
      ],
      "getDevicesFiltered": [
        {
          "path": "/{project}/global/networks",
          "method": "GET",
          "pagination": {
            "offsetVar": "",
            "limitVar": "",
            "incrementBy": "limit",
            "requestLocation": "query"
          },
          "query": {},
          "body": {},
          "headers": {},
          "handleFailure": "ignore",
          "requestFields": {
            "project": "helical-crowbar-382719"
          },
          "responseDatakey": "items",
          "responseFields": {
            "name": "{name}",
            "ostype": "{kind}",
            "ostypePrefix": "VPC Network-",
            "ipaddress": "{id}",
            "port": "{selfLinkWithId}",
            "project": "{project}"
          }
        }
      ],
      "isAlive": [
        {
          "path": "/{project}/global/networks/{network}",
          "method": "GET",
          "query": {},
          "body": {},
          "headers": {},
          "handleFailure": "ignore",
          "requestFields": {
            "project": "{project}",
            "network": ""
          },
          "responseDatakey": "",
          "responseFields": {}
        }
      ],
      "getConfig": [
        {
          "path": "/{project}/global/networks/{network}",
          "method": "GET",
          "query": {},
          "body": {},
          "headers": {},
          "handleFailure": "ignore",
          "requestFields": {
            "project": "{project}",
            "network": ""
          },
          "responseDatakey": "",
          "responseFields": {}
        }
      ],
      "getCount": [
        {
          "path": "/{project}/global/networks",
          "method": "GET",
          "query": {},
          "body": {},
          "headers": {},
          "handleFailure": "ignore",
          "requestFields": {
            "project": "helical-crowbar-382719"
          },
          "responseDatakey": "items",
          "responseFields": {}
        }
      ]
    },
    "cache": {
      "enabled": false,
      "entities": [
        {
          "entityType": "device",
          "frequency": 3600,
          "flushOnFail": false,
          "limit": 1000,
          "retryAttempts": 5,
          "sort": true,
          "populate": [
            {
              "path": "/{project}/global/networks",
              "method": "GET",
              "pagination": {
                "offsetVar": "",
                "limitVar": "",
                "incrementBy": "limit",
                "requestLocation": "query"
              },
              "query": {},
              "body": {},
              "headers": {},
              "handleFailure": "ignore",
              "requestFields": {
                "project": "helical-crowbar-382719"
              },
              "responseDatakey": "items",
              "responseFields": {
                "name": "{name}",
                "ostype": "{kind}",
                "ostypePrefix": "VPC Network-",
                "ipaddress": "{id}",
                "port": "{selfLinkWithId}",
                "project": "{project}"
              }
            }
          ],
          "cachedTasks": [
            {
              "name": "",
              "filterField": "",
              "filterLoc": ""
            }
          ]
        }
      ]
    }
  }
```
### [Swagger](https://gitlab.com/itentialopensource/adapters/adapter-gcp_compute/-/blob/master/report/adapter-openapi.json) 

## [Generic Adapter Information](https://gitlab.com/itentialopensource/adapters/adapter-gcp_compute/-/blob/master/README.md) 

