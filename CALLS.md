## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Google Cloud Platform Compute. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Google Cloud Platform Compute.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the GcpCompute. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getProject(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, callback)</td>
    <td style="padding:15px">Returns the specified Project resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectDisableXpnHost(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, callback)</td>
    <td style="padding:15px">Disable this project as a shared VPC host project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/disableXpnHost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectDisableXpnResource(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Disable a service resource (also known as service project) associated with this host project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/disableXpnResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectEnableXpnHost(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, callback)</td>
    <td style="padding:15px">Enable this project as a shared VPC host project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/enableXpnHost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectEnableXpnResource(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Enable service resource (a.k.a service project) for a host project, so that subnets in the host project can be used by instances in the service project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/enableXpnResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGetXpnHost(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, callback)</td>
    <td style="padding:15px">Gets the shared VPC host project that this project links to. May be empty if no link exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/getXpnHost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGetXpnResources(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Gets service resources (a.k.a service project) associated with this host project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/getXpnResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectListXpnHosts(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, body, callback)</td>
    <td style="padding:15px">Lists all shared VPC host projects visible to the user in an organization.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/listXpnHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectMoveDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Moves a persistent disk from one zone to another.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/moveDisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectMoveInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Moves an instance and its attached persistent disks from one zone to another.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/moveInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectSetCommonInstanceMetadata(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Sets metadata common to all instances within the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/setCommonInstanceMetadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectSetDefaultNetworkTier(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Sets the default network tier of the project. The default network tier is used when an address/forwardingRule/instance is created without specifying the network tier field.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/setDefaultNetworkTier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectSetUsageExportBucket(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Enables the usage export feature and sets the usage export bucket where reports are stored. If you provide an empty request body using this method, the usage export feature will be disabled.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/setUsageExportBucket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedAcceleratorTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of accelerator types.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/acceleratorTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneAcceleratorTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of accelerator types that are available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/acceleratorTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneAcceleratorTypesAcceleratorType(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, acceleratorType, callback)</td>
    <td style="padding:15px">Returns the specified accelerator type.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/acceleratorTypes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedAddresses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of addresses.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionAddresses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of addresses contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionAddresses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates an address resource in the specified project by using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionAddressesAddress(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, address, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified address resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/addresses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionAddressesAddress(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, address, callback)</td>
    <td style="padding:15px">Returns the specified address resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/addresses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of autoscalers.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of autoscalers contained within the specified zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, autoscaler, requestId, body, callback)</td>
    <td style="padding:15px">Updates an autoscaler in the specified project using the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, body, callback)</td>
    <td style="padding:15px">Creates an autoscaler in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectZonesZoneAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, autoscaler, requestId, body, callback)</td>
    <td style="padding:15px">Updates an autoscaler in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneAutoscalersAutoscaler(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, autoscaler, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified autoscaler.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/autoscalers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneAutoscalersAutoscaler(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, autoscaler, callback)</td>
    <td style="padding:15px">Returns the specified autoscaler resource. Gets a list of available autoscalers by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/autoscalers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedBackendServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of all BackendService resources, regional and global, available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/backendServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalBackendServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of BackendService resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a BackendService resource in the specified project using the data included in the request. For more information, see  Backend services overview.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified BackendService resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, callback)</td>
    <td style="padding:15px">Returns the specified BackendService resource. Gets a list of available backend services.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified BackendService resource with the data included in the request. For more information, see  Backend services overview. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified BackendService resource with the data included in the request. For more information, see Backend services overview.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendServicesBackendServiceAddSignedUrlKey(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, requestId, body, callback)</td>
    <td style="padding:15px">Adds a key for validating requests with signed URLs for this backend service.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}/addSignedUrlKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendServicesBackendServiceDeleteSignedUrlKey(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, keyName, requestId, callback)</td>
    <td style="padding:15px">Deletes a key for validating requests with signed URLs for this backend service.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}/deleteSignedUrlKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendServicesBackendServiceGetHealth(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, body, callback)</td>
    <td style="padding:15px">Gets the most recent health check results for this BackendService.

Example request body:

{ "group": "/zones/us-east1-b/instanceGroups/lb-backend-example" }</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}/getHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendServicesBackendServiceSetSecurityPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendService, requestId, body, callback)</td>
    <td style="padding:15px">Sets the security policy for the specified backend service.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendServices/{pathv2}/setSecurityPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedCommitments(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of commitments.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/commitments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionCommitments(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of commitments contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/commitments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionCommitments(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a commitment in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/commitments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionCommitmentsCommitment(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, commitment, callback)</td>
    <td style="padding:15px">Returns the specified commitment resource. Gets a list of available commitments by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/commitments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedDiskTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of disk types.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/diskTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneDiskTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of disk types available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/diskTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneDiskTypesDiskType(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, diskType, callback)</td>
    <td style="padding:15px">Returns the specified disk type. Gets a list of available disk types by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/diskTypes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedDisks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of persistent disks.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/disks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneDisks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of persistent disks contained within the specified zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, sourceImage, body, callback)</td>
    <td style="padding:15px">Creates a persistent disk in the specified project using the data in the request. You can create a disk from a source (sourceImage, sourceSnapshot, or sourceDisk) or create an empty 500 GB data disk by omitting all properties. You can also create a disk that is larger than the default size by specifying the sizeGb property.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneDisksDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, disk, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified persistent disk. Deleting a disk removes its data permanently and is irreversible. However, deleting a disk does not delete any snapshots previously made from the disk. You must separately delete snapshots.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneDisksDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, disk, callback)</td>
    <td style="padding:15px">Returns a specified persistent disk. Gets a list of available persistent disks by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksDiskAddResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, disk, requestId, body, callback)</td>
    <td style="padding:15px">Adds existing resource policies to a disk. You can only add one policy which will be applied to this disk for scheduling snapshot creation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/addResourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksDiskCreateSnapshot(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, disk, guestFlush, requestId, body, callback)</td>
    <td style="padding:15px">Creates a snapshot of a specified persistent disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/createSnapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksDiskRemoveResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, disk, requestId, body, callback)</td>
    <td style="padding:15px">Removes resource policies from a disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/removeResourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksDiskResize(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, disk, requestId, body, callback)</td>
    <td style="padding:15px">Resizes the specified persistent disk. You can only increase the size of the disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneDisksResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksResourceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, requestId, body, callback)</td>
    <td style="padding:15px">Sets the labels on a disk. To learn more about labels, read the Labeling Resources documentation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneDisksResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/disks/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedForwardingRules(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of forwarding rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionForwardingRules(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of ForwardingRule resources available to the specified project and region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionForwardingRules(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a ForwardingRule resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionForwardingRulesForwardingRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, forwardingRule, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified ForwardingRule resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/forwardingRules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionForwardingRulesForwardingRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, forwardingRule, callback)</td>
    <td style="padding:15px">Returns the specified ForwardingRule resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/forwardingRules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionForwardingRulesForwardingRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, forwardingRule, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified forwarding rule with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules. Currently, you can only patch the network_tier field.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/forwardingRules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionForwardingRulesForwardingRuleSetTarget(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, forwardingRule, requestId, body, callback)</td>
    <td style="padding:15px">Changes target URL for forwarding rule. The new target should be of the same type as the old target.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/forwardingRules/{pathv3}/setTarget?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of all HealthCheck resources, regional and global, available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/healthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of HealthCheck resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/healthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a HealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/healthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, healthCheck, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified HealthCheck resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/healthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, healthCheck, callback)</td>
    <td style="padding:15px">Returns the specified HealthCheck resource. Gets a list of available health checks by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/healthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, healthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HealthCheck resource in the specified project using the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/healthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, healthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/healthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedInstanceGroupManagers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of managed instance groups and groups them by zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/instanceGroupManagers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstanceGroupManagers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of managed instance groups that are contained within the specified project and zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, body, callback)</td>
    <td style="padding:15px">Creates a managed instance group using the information that you specify in the request. After the group is created, instances in the group are created using the specified instance template. This operation is marked as DONE when the group is created even if the instances in the group have not yet been created. You must separately verify the status of the individual instances with the listmanagedinstances method.

A managed instance group can have up to 1000 VM instances per group. Please contact ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneInstanceGroupManagersInstanceGroupManager(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified managed instance group and all of the instances in that group. Note that the instance group must not belong to a backend service. Read  Deleting an instance group for more information.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstanceGroupManagersInstanceGroupManager(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, callback)</td>
    <td style="padding:15px">Returns all of the details about the specified managed instance group. Gets a list of available managed instance groups by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneInstanceGroupManagersInstanceGroupManager(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Updates a managed instance group using the information that you specify in the request. This operation is marked as DONE when the group is patched even if the instances in the group are still in the process of being patched. You must separately verify the status of the individual instances with the listManagedInstances method. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerAbandonInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Flags the specified instances to be removed from the managed instance group. Abandoning an instance does not delete the instance, but it does remove the instance from any target pools that are applied by the managed instance group. This method reduces the targetSize of the managed instance group by the number of instances that you abandon. This operation is marked as DONE when the action is scheduled even if the instances have not yet been removed from the group. You must separately verify the s...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/abandonInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerApplyUpdatesToInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, body, callback)</td>
    <td style="padding:15px">Applies changes to selected instances on the managed instance group. This method can be used to apply new overrides and/or new versions.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/applyUpdatesToInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerCreateInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Creates instances with per-instance configs in this managed instance group. Instances are created using the current instance template. The create instances operation is marked DONE if the createInstances request is successful. The underlying actions take additional time. You must separately verify the status of the creating or actions with the listmanagedinstances method.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/createInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerDeleteInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Flags the specified instances in the managed instance group for immediate deletion. The instances are also removed from any target pools of which they were a member. This method reduces the targetSize of the managed instance group by the number of instances that you delete. This operation is marked as DONE when the action is scheduled even if the instances are still being deleted. You must separately verify the status of the deleting action with the listmanagedinstances method.

If the group is ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/deleteInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerDeletePerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, body, callback)</td>
    <td style="padding:15px">Deletes selected per-instance configs for the managed instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/deletePerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstanceGroupManagersInstanceGroupManagerListErrors(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all errors thrown by actions on instances for a given managed instance group. The filter and orderBy query parameters are not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/listErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerListManagedInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all of the instances in the managed instance group. Each instance in the list has a currentAction, which indicates the action that the managed instance group is performing on the instance. For example, if the group is still creating an instance, the currentAction is CREATING. If a previous action failed, the list displays the errors for that failed action. The orderBy query parameter is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/listManagedInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerListPerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all of the per-instance configs defined for the managed instance group. The orderBy query parameter is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/listPerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerPatchPerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Inserts or patches per-instance configs for the managed instance group. perInstanceConfig.name serves as a key used to distinguish whether to perform insert or patch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/patchPerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerRecreateInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Flags the specified instances in the managed instance group to be immediately recreated. The instances are deleted and recreated using the current instance template for the managed instance group. This operation is marked as DONE when the flag is set even if the instances have not yet been recreated. You must separately verify the status of the recreating action with the listmanagedinstances method.

If the group is part of a backend service that has enabled connection draining, it can take up t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/recreateInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerResize(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, size, requestId, callback)</td>
    <td style="padding:15px">Resizes the managed instance group. If you increase the size, the group creates new instances using the current instance template. If you decrease the size, the group deletes instances. The resize operation is marked DONE when the resize actions are scheduled even if the group has not yet added or deleted any instances. You must separately verify the status of the creating or deleting actions with the listmanagedinstances method.

When resizing down, the instance group arbitrarily chooses the or...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerSetInstanceTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Specifies the instance template to use when creating new instances in this group. The templates for existing instances in the group do not change unless you recreate them.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/setInstanceTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerSetTargetPools(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Modifies the target pools to which all instances in this managed instance group are assigned. The target pools automatically apply to all of the instances in the managed instance group. This operation is marked DONE when you make the request even if the instances have not yet been added to their target pools. The change might take some time to apply to all of the instances in the group depending on the size of the group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/setTargetPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupManagersInstanceGroupManagerUpdatePerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Inserts or updates per-instance configs for the managed instance group. perInstanceConfig.name serves as a key used to distinguish whether to perform insert or patch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroupManagers/{pathv3}/updatePerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedInstanceGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of instance groups and sorts them by zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/instanceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstanceGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of instance groups that are located in the specified project and zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, body, callback)</td>
    <td style="padding:15px">Creates an instance group in the specified project using the parameters that are included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneInstanceGroupsInstanceGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroup, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified instance group. The instances in the group are not deleted. Note that instance group must not belong to a backend service. Read  Deleting an instance group for more information.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstanceGroupsInstanceGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroup, callback)</td>
    <td style="padding:15px">Returns the specified instance group. Gets a list of available instance groups by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupsInstanceGroupAddInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroup, requestId, body, callback)</td>
    <td style="padding:15px">Adds a list of instances to the specified instance group. All of the instances in the instance group must be in the same network/subnetwork. Read  Adding instances for more information.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups/{pathv3}/addInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupsInstanceGroupListInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroup, filter, maxResults, orderBy, pageToken, body, callback)</td>
    <td style="padding:15px">Lists the instances in the specified instance group. The orderBy query parameter is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups/{pathv3}/listInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupsInstanceGroupRemoveInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroup, requestId, body, callback)</td>
    <td style="padding:15px">Removes one or more instances from the specified instance group, but does not delete those instances.

If the group is part of a backend service that has enabled connection draining, it can take up to 60 seconds after the connection draining duration before the VM instance is removed or deleted.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups/{pathv3}/removeInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstanceGroupsInstanceGroupSetNamedPorts(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instanceGroup, requestId, body, callback)</td>
    <td style="padding:15px">Sets the named ports for the specified instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instanceGroups/{pathv3}/setNamedPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves aggregated list of all of the instances in your project across all regions and zones.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of instances contained within the specified zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, sourceInstanceTemplate, body, callback)</td>
    <td style="padding:15px">Creates an instance resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneInstancesInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified Instance resource. For more information, see Stopping or Deleting an Instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, callback)</td>
    <td style="padding:15px">Returns the specified Instance resource. Gets a list of available instances by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectZonesZoneInstancesInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, minimalAction = 'INVALID', mostDisruptiveAllowedAction = 'INVALID', requestId, body, callback)</td>
    <td style="padding:15px">Updates an instance only if the necessary resources are available. This method can update only a specific set of instance properties. See  Updating a running instance for a list of updatable instance properties.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceAddAccessConfig(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, networkInterface, requestId, body, callback)</td>
    <td style="padding:15px">Adds an access config to an instance's network interface.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/addAccessConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceAddResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Adds existing resource policies to an instance. You can only add one policy right now which will be applied to this instance for scheduling live migrations.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/addResourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceAttachDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, forceAttach, requestId, body, callback)</td>
    <td style="padding:15px">Attaches an existing Disk resource to an instance. You must first create the disk before you can attach it. It is not possible to create and attach a disk at the same time. For more information, read Adding a persistent disk to your instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/attachDisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceDeleteAccessConfig(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, accessConfig, networkInterface, requestId, callback)</td>
    <td style="padding:15px">Deletes an access config from an instance's network interface.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/deleteAccessConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceDetachDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, deviceName, requestId, callback)</td>
    <td style="padding:15px">Detaches a disk from an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/detachDisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesInstanceGetGuestAttributes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, queryPath, variableKey, callback)</td>
    <td style="padding:15px">Returns the specified guest attributes entry.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/getGuestAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesInstanceGetShieldedInstanceIdentity(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, callback)</td>
    <td style="padding:15px">Returns the Shielded Instance Identity of an instance</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/getShieldedInstanceIdentity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesInstanceReferrers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of resources that refer to the VM instance specified in the request. For example, if the VM instance is part of a managed instance group, the referrers list includes the managed instance group. For more information, read Viewing Referrers to VM Instances.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/referrers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceRemoveResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Removes resource policies from an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/removeResourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceReset(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, callback)</td>
    <td style="padding:15px">Performs a reset on the instance. This is a hard reset the VM does not do a graceful shutdown. For more information, see Resetting an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesInstanceScreenshot(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, callback)</td>
    <td style="padding:15px">Returns the screenshot from the specified instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/screenshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesInstanceSerialPort(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, port, start, callback)</td>
    <td style="padding:15px">Returns the last 1 MB of serial port output from the specified instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/serialPort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetDiskAutoDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, autoDelete, deviceName, requestId, callback)</td>
    <td style="padding:15px">Sets the auto-delete flag for a disk attached to an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setDiskAutoDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Sets labels on an instance. To learn more about labels, read the Labeling Resources documentation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetMachineResources(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Changes the number and/or type of accelerator for a stopped instance to the values specified in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setMachineResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetMachineType(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Changes the machine type for a stopped instance to the machine type specified in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setMachineType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetMetadata(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Sets metadata for the specified instance to the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setMetadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetMinCpuPlatform(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Changes the minimum CPU platform that this instance should use. This method can only be called on a stopped instance. For more information, read Specifying a Minimum CPU Platform.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setMinCpuPlatform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetScheduling(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Sets an instance's scheduling options. You can only call this method on a stopped instance, that is, a VM instance that is in a `TERMINATED` state. See Instance Life Cycle for more information on the possible instance states.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setScheduling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetServiceAccount(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Sets the service account on the instance. For more information, read Changing the service account and access scopes for an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setServiceAccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneInstancesInstanceSetShieldedInstanceIntegrityPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Sets the Shielded Instance integrity policy for an instance. You can only use this method on a running instance. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setShieldedInstanceIntegrityPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSetTags(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Sets network tags for the specified instance to the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceSimulateMaintenanceEvent(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, callback)</td>
    <td style="padding:15px">Simulates a maintenance event on the instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/simulateMaintenanceEvent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceStart(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, callback)</td>
    <td style="padding:15px">Starts an instance that was stopped using the instances().stop method. For more information, see Restart an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceStartWithEncryptionKey(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Starts an instance that was stopped using the instances().stop method. For more information, see Restart an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/startWithEncryptionKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceStop(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, callback)</td>
    <td style="padding:15px">Stops a running instance, shutting it down cleanly, and allows you to restart the instance at a later time. Stopped instances do not incur VM usage charges while they are stopped. However, resources that the VM is using, such as persistent disks and static IP addresses, will continue to be charged until they are deleted. For more information, see Stopping an instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesInstanceUpdateAccessConfig(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, networkInterface, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified access config from an instance's network interface with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/updateAccessConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneInstancesInstanceUpdateDisplayDevice(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Updates the Display config for a VM instance. You can only use this method on a stopped VM instance. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/updateDisplayDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneInstancesInstanceUpdateNetworkInterface(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, networkInterface, requestId, body, callback)</td>
    <td style="padding:15px">Updates an instance's network interface. This method follows PATCH semantics.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/updateNetworkInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneInstancesInstanceUpdateShieldedInstanceConfig(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, instance, requestId, body, callback)</td>
    <td style="padding:15px">Updates the Shielded Instance config for an instance. You can only use this method on a stopped instance. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/updateShieldedInstanceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneInstancesResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesResourceSetDeletionProtection(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, deletionProtection, requestId, callback)</td>
    <td style="padding:15px">Sets deletion protection on the instance.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setDeletionProtection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneInstancesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/instances/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedInterconnectAttachments(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of interconnect attachments.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/interconnectAttachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInterconnectAttachments(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of interconnect attachments contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/interconnectAttachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInterconnectAttachments(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, validateOnly, body, callback)</td>
    <td style="padding:15px">Creates an InterconnectAttachment in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/interconnectAttachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionInterconnectAttachmentsInterconnectAttachment(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, interconnectAttachment, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified interconnect attachment.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/interconnectAttachments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInterconnectAttachmentsInterconnectAttachment(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, interconnectAttachment, callback)</td>
    <td style="padding:15px">Returns the specified interconnect attachment.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/interconnectAttachments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionInterconnectAttachmentsInterconnectAttachment(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, interconnectAttachment, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified interconnect attachment with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/interconnectAttachments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedMachineTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of machine types.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/machineTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneMachineTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of machine types available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/machineTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneMachineTypesMachineType(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, machineType, callback)</td>
    <td style="padding:15px">Returns the specified machine type. Gets a list of available machine types by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/machineTypes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of network endpoint groups and sorts them by zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of network endpoint groups that are located in the specified project and zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, body, callback)</td>
    <td style="padding:15px">Creates a network endpoint group in the specified project using the parameters that are included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneNetworkEndpointGroupsNetworkEndpointGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, networkEndpointGroup, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified network endpoint group. The network endpoints in the NEG and the VM instances they belong to are not terminated when the NEG is deleted. Note that the NEG cannot be deleted if there are backend services referencing it.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNetworkEndpointGroupsNetworkEndpointGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, networkEndpointGroup, callback)</td>
    <td style="padding:15px">Returns the specified network endpoint group. Gets a list of available network endpoint groups by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNetworkEndpointGroupsNetworkEndpointGroupAttachNetworkEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, networkEndpointGroup, requestId, body, callback)</td>
    <td style="padding:15px">Attach a list of network endpoints to the specified network endpoint group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups/{pathv3}/attachNetworkEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNetworkEndpointGroupsNetworkEndpointGroupDetachNetworkEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, networkEndpointGroup, requestId, body, callback)</td>
    <td style="padding:15px">Detach a list of network endpoints from the specified network endpoint group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups/{pathv3}/detachNetworkEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNetworkEndpointGroupsNetworkEndpointGroupListNetworkEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, networkEndpointGroup, filter, maxResults, orderBy, pageToken, body, callback)</td>
    <td style="padding:15px">Lists the network endpoints in the specified network endpoint group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups/{pathv3}/listNetworkEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNetworkEndpointGroupsResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/networkEndpointGroups/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedNodeGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of node groups. Note: use nodeGroups.listNodes for more details about each group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/nodeGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNodeGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of node groups available to the specified project. Note: use nodeGroups.listNodes for more details about each group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, initialNodeCount, requestId, body, callback)</td>
    <td style="padding:15px">Creates a NodeGroup resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneNodeGroupsNodeGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified NodeGroup resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNodeGroupsNodeGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, callback)</td>
    <td style="padding:15px">Returns the specified NodeGroup. Get a list of available NodeGroups by making a list() request. Note: the "nodes" field should not be used. Use nodeGroups.listNodes instead.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectZonesZoneNodeGroupsNodeGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified node group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroupsNodeGroupAddNodes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, requestId, body, callback)</td>
    <td style="padding:15px">Adds specified number of nodes to the node group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/addNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroupsNodeGroupDeleteNodes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, requestId, body, callback)</td>
    <td style="padding:15px">Deletes specified nodes from the node group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/deleteNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroupsNodeGroupListNodes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists nodes in the node group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/listNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroupsNodeGroupSetNodeTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeGroup, requestId, body, callback)</td>
    <td style="padding:15px">Updates the node template of the node group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/setNodeTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNodeGroupsResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroupsResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneNodeGroupsResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeGroups/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedNodeTemplates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of node templates.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/nodeTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNodeTemplates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of node templates available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionNodeTemplates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a NodeTemplate resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionNodeTemplatesNodeTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, nodeTemplate, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified NodeTemplate resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNodeTemplatesNodeTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, nodeTemplate, callback)</td>
    <td style="padding:15px">Returns the specified node template. Gets a list of available node templates by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNodeTemplatesResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionNodeTemplatesResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionNodeTemplatesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/nodeTemplates/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedNodeTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of node types.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/nodeTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNodeTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of node types available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneNodeTypesNodeType(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, nodeType, callback)</td>
    <td style="padding:15px">Returns the specified node type. Gets a list of available node types by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/nodeTypes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedOperations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of all operations.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalOperations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of Operation resources contained within the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalOperationsOperation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, operation, callback)</td>
    <td style="padding:15px">Deletes the specified Operations resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalOperationsOperation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, operation, callback)</td>
    <td style="padding:15px">Retrieves the specified Operations resource. Gets a list of operations by making a `list()` request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalOperationsOperationWait(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, operation, callback)</td>
    <td style="padding:15px">Waits for the specified Operation resource to return as `DONE` or for the request to approach the 2 minute deadline, and retrieves the specified Operation resource. This method differs from the `GET` method in that it waits for no more than the default deadline (2 minutes) and then returns the current state of the operation, which might be `DONE` or still in progress.

This method is called on a best-effort basis. Specifically:
- In uncommon cases, when the server is overloaded, the request mi...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/operations/{pathv2}/wait?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedPacketMirrorings(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of packetMirrorings.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/packetMirrorings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionPacketMirrorings(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of PacketMirroring resources available to the specified project and region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/packetMirrorings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionPacketMirrorings(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a PacketMirroring resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/packetMirrorings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionPacketMirroringsPacketMirroring(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, packetMirroring, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified PacketMirroring resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/packetMirrorings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionPacketMirroringsPacketMirroring(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, packetMirroring, callback)</td>
    <td style="padding:15px">Returns the specified PacketMirroring resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/packetMirrorings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionPacketMirroringsPacketMirroring(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, packetMirroring, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified PacketMirroring resource with the data included in the request. This method supports PATCH semantics and uses JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/packetMirrorings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionPacketMirroringsResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/packetMirrorings/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedReservations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of reservations.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/reservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneReservations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">A list of all the reservations that have been configured for the specified project in specified zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneReservations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, body, callback)</td>
    <td style="padding:15px">Creates a new reservation. For more information, read Reserving zonal resources.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneReservationsReservation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, reservation, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified reservation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneReservationsReservation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, reservation, callback)</td>
    <td style="padding:15px">Retrieves information about the specified reservation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneReservationsReservationResize(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, reservation, requestId, body, callback)</td>
    <td style="padding:15px">Resizes the reservation (applicable to standalone reservations only). For more information, read Modifying reservations.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations/{pathv3}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneReservationsResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneReservationsResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneReservationsResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/reservations/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of resource policies.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/resourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">A list all the resource policies that have been configured for the specified project in specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a new resource policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionResourcePoliciesResourcePolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resourcePolicy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified resource policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionResourcePoliciesResourcePolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resourcePolicy, callback)</td>
    <td style="padding:15px">Retrieves all information of the specified resource policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionResourcePoliciesResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionResourcePoliciesResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionResourcePoliciesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/resourcePolicies/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedRouters(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of routers.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionRouters(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of Router resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionRouters(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a Router resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionRoutersRouter(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified Router resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionRoutersRouter(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, callback)</td>
    <td style="padding:15px">Returns the specified Router resource. Gets a list of available routers by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionRoutersRouter(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified Router resource with the data included in the request. This method supports PATCH semantics and uses JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectRegionsRegionRoutersRouter(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified Router resource with the data included in the request. This method conforms to PUT semantics, which requests that the state of the target resource be created or replaced with the state defined by the representation enclosed in the request message payload.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionRoutersRouterGetNatMappingInfo(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves runtime Nat mapping information of VM endpoints.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}/getNatMappingInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionRoutersRouterGetRouterStatus(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, callback)</td>
    <td style="padding:15px">Retrieves runtime information of the specified router.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}/getRouterStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionRoutersRouterPreview(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, router, body, callback)</td>
    <td style="padding:15px">Preview fields auto-generated during router create and update operations. Calling this method does NOT create or update the router.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/routers/{pathv3}/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of all SslCertificate resources, regional and global, available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/sslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of SslCertificate resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a SslCertificate resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalSslCertificatesSslCertificate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, sslCertificate, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified SslCertificate resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslCertificates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSslCertificatesSslCertificate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, sslCertificate, callback)</td>
    <td style="padding:15px">Returns the specified SslCertificate resource. Gets a list of available SSL certificates by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslCertificates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedSubnetworks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of subnetworks.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/subnetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedSubnetworksListUsable(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of all usable subnetworks in the project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/subnetworks/listUsable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionSubnetworks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of subnetworks available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionSubnetworks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a subnetwork in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionSubnetworksResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionSubnetworksResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionSubnetworksResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionSubnetworksSubnetwork(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, subnetwork, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified subnetwork.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionSubnetworksSubnetwork(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, subnetwork, callback)</td>
    <td style="padding:15px">Returns the specified subnetwork. Gets a list of available subnetworks list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionSubnetworksSubnetwork(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, subnetwork, drainTimeoutSeconds, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified subnetwork with the data included in the request. Only certain fields can up updated with a patch request as indicated in the field descriptions. You must specify the current fingerprint of the subnetwork resource being patched.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionSubnetworksSubnetworkExpandIpCidrRange(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, subnetwork, requestId, body, callback)</td>
    <td style="padding:15px">Expands the IP CIDR range of the subnetwork to a specified value.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}/expandIpCidrRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionSubnetworksSubnetworkSetPrivateIpGoogleAccess(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, subnetwork, requestId, body, callback)</td>
    <td style="padding:15px">Set whether VMs in this subnet can access Google services without assigning external IP addresses through Private Google Access.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/subnetworks/{pathv3}/setPrivateIpGoogleAccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedTargetHttpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of all TargetHttpProxy resources, regional and global, available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/targetHttpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetHttpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of TargetHttpProxy resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetHttpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetHttpProxy resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalTargetHttpProxiesTargetHttpProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetHttpProxy resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetHttpProxiesTargetHttpProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetHttpProxy resource. Gets a list of available target HTTP proxies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectTargetHttpProxiesTargetHttpProxySetUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the URL map for TargetHttpProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/targetHttpProxies/{pathv2}/setUrlMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedTargetHttpsProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of all TargetHttpsProxy resources, regional and global, available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/targetHttpsProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetHttpsProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of TargetHttpsProxy resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpsProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetHttpsProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetHttpsProxy resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpsProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalTargetHttpsProxiesTargetHttpsProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpsProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetHttpsProxy resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpsProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetHttpsProxiesTargetHttpsProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpsProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetHttpsProxy resource. Gets a list of available target HTTPS proxies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpsProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetHttpsProxiesTargetHttpsProxySetQuicOverride(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpsProxy, requestId, body, callback)</td>
    <td style="padding:15px">Sets the QUIC override policy for TargetHttpsProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpsProxies/{pathv2}/setQuicOverride?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetHttpsProxiesTargetHttpsProxySetSslPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpsProxy, requestId, body, callback)</td>
    <td style="padding:15px">Sets the SSL policy for TargetHttpsProxy. The SSL policy specifies the server-side support for SSL features. This affects connections between clients and the HTTPS proxy load balancer. They do not affect the connection between the load balancer and the backends.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetHttpsProxies/{pathv2}/setSslPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectTargetHttpsProxiesTargetHttpsProxySetSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpsProxy, requestId, body, callback)</td>
    <td style="padding:15px">Replaces SslCertificates for TargetHttpsProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/targetHttpsProxies/{pathv2}/setSslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectTargetHttpsProxiesTargetHttpsProxySetUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetHttpsProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the URL map for TargetHttpsProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/targetHttpsProxies/{pathv2}/setUrlMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedTargetInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of target instances.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/targetInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneTargetInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of TargetInstance resources available to the specified project and zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/targetInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneTargetInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetInstance resource in the specified project and zone using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/targetInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneTargetInstancesTargetInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, targetInstance, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetInstance resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/targetInstances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneTargetInstancesTargetInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, targetInstance, callback)</td>
    <td style="padding:15px">Returns the specified TargetInstance resource. Gets a list of available target instances by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/targetInstances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedTargetPools(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of target pools.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/targetPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetPools(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of target pools available to the specified project and region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPools(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a target pool in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionTargetPoolsTargetPool(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified target pool.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetPoolsTargetPool(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, callback)</td>
    <td style="padding:15px">Returns the specified target pool. Gets a list of available target pools by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPoolsTargetPoolAddHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, requestId, body, callback)</td>
    <td style="padding:15px">Adds health check URLs to a target pool.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}/addHealthCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPoolsTargetPoolAddInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, requestId, body, callback)</td>
    <td style="padding:15px">Adds an instance to a target pool.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}/addInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPoolsTargetPoolGetHealth(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, body, callback)</td>
    <td style="padding:15px">Gets the most recent health check results for each IP for the instance that is referenced by the given target pool.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}/getHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPoolsTargetPoolRemoveHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, requestId, body, callback)</td>
    <td style="padding:15px">Removes health check URL from a target pool.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}/removeHealthCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPoolsTargetPoolRemoveInstance(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, requestId, body, callback)</td>
    <td style="padding:15px">Removes instance URL from a target pool.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}/removeInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetPoolsTargetPoolSetBackup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetPool, failoverRatio, requestId, body, callback)</td>
    <td style="padding:15px">Changes a backup target pool's configurations.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetPools/{pathv3}/setBackup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedTargetVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of target VPN gateways.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/targetVpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of target VPN gateways available to the specified project and region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetVpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a target VPN gateway in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetVpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionTargetVpnGatewaysTargetVpnGateway(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetVpnGateway, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified target VPN gateway.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetVpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetVpnGatewaysTargetVpnGateway(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetVpnGateway, callback)</td>
    <td style="padding:15px">Returns the specified target VPN gateway. Gets a list of available target VPN gateways by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetVpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedUrlMaps(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of all UrlMap resources, regional and global, available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/urlMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalUrlMaps(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of UrlMap resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalUrlMaps(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a UrlMap resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, urlMap, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified UrlMap resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, urlMap, callback)</td>
    <td style="padding:15px">Returns the specified UrlMap resource. Gets a list of available URL maps by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, urlMap, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified UrlMap resource with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, urlMap, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified UrlMap resource with the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalUrlMapsUrlMapInvalidateCache(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, urlMap, requestId, body, callback)</td>
    <td style="padding:15px">Initiates a cache invalidation operation, invalidating the specified path, scoped to the specified UrlMap.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps/{pathv2}/invalidateCache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalUrlMapsUrlMapValidate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, urlMap, body, callback)</td>
    <td style="padding:15px">Runs static validation for the UrlMap. In particular, the tests of the provided UrlMap will be run. Calling this method does NOT create the UrlMap.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/urlMaps/{pathv2}/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of VPN gateways.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/vpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of VPN gateways available to the specified project and region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a VPN gateway in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionVpnGatewaysResourceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, requestId, body, callback)</td>
    <td style="padding:15px">Sets the labels on a VpnGateway. To learn more about labels, read the Labeling Resources documentation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways/{pathv3}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionVpnGatewaysResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionVpnGatewaysVpnGateway(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, vpnGateway, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified VPN gateway.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionVpnGatewaysVpnGateway(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, vpnGateway, callback)</td>
    <td style="padding:15px">Returns the specified VPN gateway. Gets a list of available VPN gateways by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionVpnGatewaysVpnGatewayGetStatus(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, vpnGateway, callback)</td>
    <td style="padding:15px">Returns the status for the specified VPN gateway.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnGateways/{pathv3}/getStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAggregatedVpnTunnels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, includeAllScopes, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves an aggregated list of VPN tunnels.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/aggregated/vpnTunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionVpnTunnels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of VpnTunnel resources contained in the specified project and region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnTunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionVpnTunnels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a VpnTunnel resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnTunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionVpnTunnelsVpnTunnel(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, vpnTunnel, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified VpnTunnel resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnTunnels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionVpnTunnelsVpnTunnel(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, vpnTunnel, callback)</td>
    <td style="padding:15px">Returns the specified VpnTunnel resource. Gets a list of available VPN tunnels by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/vpnTunnels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalAddresses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of global addresses.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalAddresses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates an address resource in the specified project by using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalAddressesAddress(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, address, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified address resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/addresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalAddressesAddress(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, address, callback)</td>
    <td style="padding:15px">Returns the specified address resource. Gets a list of available addresses by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/addresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalBackendBuckets(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of BackendBucket resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendBuckets(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a BackendBucket resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalBackendBucketsBackendBucket(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendBucket, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified BackendBucket resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalBackendBucketsBackendBucket(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendBucket, callback)</td>
    <td style="padding:15px">Returns the specified BackendBucket resource. Gets a list of available backend buckets by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalBackendBucketsBackendBucket(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendBucket, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified BackendBucket resource with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalBackendBucketsBackendBucket(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendBucket, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified BackendBucket resource with the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendBucketsBackendBucketAddSignedUrlKey(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendBucket, requestId, body, callback)</td>
    <td style="padding:15px">Adds a key for validating requests with signed URLs for this backend bucket.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets/{pathv2}/addSignedUrlKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalBackendBucketsBackendBucketDeleteSignedUrlKey(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, backendBucket, keyName, requestId, callback)</td>
    <td style="padding:15px">Deletes a key for validating requests with signed URLs for this backend bucket.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/backendBuckets/{pathv2}/deleteSignedUrlKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalExternalVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of ExternalVpnGateway available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/externalVpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalExternalVpnGateways(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a ExternalVpnGateway in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/externalVpnGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalExternalVpnGatewaysExternalVpnGateway(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, externalVpnGateway, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified externalVpnGateway.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/externalVpnGateways/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalExternalVpnGatewaysExternalVpnGateway(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, externalVpnGateway, callback)</td>
    <td style="padding:15px">Returns the specified externalVpnGateway. Get a list of available externalVpnGateways by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/externalVpnGateways/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalExternalVpnGatewaysResourceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the labels on an ExternalVpnGateway. To learn more about labels, read the Labeling Resources documentation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/externalVpnGateways/{pathv2}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalExternalVpnGatewaysResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/externalVpnGateways/{pathv2}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalFirewalls(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of firewall rules available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/firewalls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalFirewalls(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a firewall rule in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/firewalls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalFirewallsFirewall(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, firewall, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified firewall.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/firewalls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalFirewallsFirewall(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, firewall, callback)</td>
    <td style="padding:15px">Returns the specified firewall.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/firewalls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalFirewallsFirewall(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, firewall, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified firewall rule with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/firewalls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalFirewallsFirewall(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, firewall, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified firewall rule with the data included in the request. Note that all fields will be updated if using PUT, even fields that are not specified. To update individual fields, please use PATCH instead.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/firewalls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalForwardingRules(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of GlobalForwardingRule resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalForwardingRules(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a GlobalForwardingRule resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalForwardingRulesForwardingRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, forwardingRule, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified GlobalForwardingRule resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/forwardingRules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalForwardingRulesForwardingRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, forwardingRule, callback)</td>
    <td style="padding:15px">Returns the specified GlobalForwardingRule resource. Gets a list of available forwarding rules by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/forwardingRules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalForwardingRulesForwardingRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, forwardingRule, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified forwarding rule with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules. Currently, you can only patch the network_tier field.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/forwardingRules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalForwardingRulesForwardingRuleSetTarget(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, forwardingRule, requestId, body, callback)</td>
    <td style="padding:15px">Changes target URL for the GlobalForwardingRule resource. The new target should be of the same type as the old target.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/forwardingRules/{pathv2}/setTarget?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalHttpHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of HttpHealthCheck resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpHealthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalHttpHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a HttpHealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpHealthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalHttpHealthChecksHttpHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpHealthCheck, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified HttpHealthCheck resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalHttpHealthChecksHttpHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpHealthCheck, callback)</td>
    <td style="padding:15px">Returns the specified HttpHealthCheck resource. Gets a list of available HTTP health checks by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalHttpHealthChecksHttpHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpHealthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HttpHealthCheck resource in the specified project using the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalHttpHealthChecksHttpHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpHealthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HttpHealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalHttpsHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of HttpsHealthCheck resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpsHealthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalHttpsHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a HttpsHealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpsHealthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalHttpsHealthChecksHttpsHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpsHealthCheck, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified HttpsHealthCheck resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpsHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalHttpsHealthChecksHttpsHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpsHealthCheck, callback)</td>
    <td style="padding:15px">Returns the specified HttpsHealthCheck resource. Gets a list of available HTTPS health checks by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpsHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalHttpsHealthChecksHttpsHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpsHealthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HttpsHealthCheck resource in the specified project using the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpsHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectGlobalHttpsHealthChecksHttpsHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, httpsHealthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HttpsHealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/httpsHealthChecks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalImages(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of custom images available to the specified project. Custom images are images you create that belong to your project. This method does not get any images that belong to other projects, including publicly-available images, like Debian 8. If you want to get a list of publicly-available images, use this method to make a request to the respective image project, such as debian-cloud or windows-cloud.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalImages(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, forceCreate, requestId, body, callback)</td>
    <td style="padding:15px">Creates an image in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalImagesFamilyFamily(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, family, callback)</td>
    <td style="padding:15px">Returns the latest image that is part of an image family and is not deprecated.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/family/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalImagesImage(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, image, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified image.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalImagesImage(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, image, callback)</td>
    <td style="padding:15px">Returns the specified image. Gets a list of available images by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalImagesImageDeprecate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, image, requestId, body, callback)</td>
    <td style="padding:15px">Sets the deprecation status of an image.

If an empty request body is given, clears the deprecation status instead.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}/deprecate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalImagesResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalImagesResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalImagesResourceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the labels on an image. To learn more about labels, read the Labeling Resources documentation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalImagesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/images/{pathv2}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInstanceTemplates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of instance templates that are contained within the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalInstanceTemplates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates an instance template in the specified project using the data that is included in the request. If you are creating a new template to update an existing instance group, your new instance template must use the same network or, if applicable, the same subnetwork as the original template.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalInstanceTemplatesInstanceTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, instanceTemplate, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified instance template. Deleting an instance template is permanent and cannot be undone. It is not possible to delete templates that are already in use by a managed instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInstanceTemplatesInstanceTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, instanceTemplate, callback)</td>
    <td style="padding:15px">Returns the specified instance template. Gets a list of available instance templates by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInstanceTemplatesResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates/{pathv2}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalInstanceTemplatesResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates/{pathv2}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalInstanceTemplatesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/instanceTemplates/{pathv2}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInterconnectLocations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of interconnect locations available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnectLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInterconnectLocationsInterconnectLocation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, interconnectLocation, callback)</td>
    <td style="padding:15px">Returns the details for the specified interconnect location. Gets a list of available interconnect locations by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnectLocations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInterconnects(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of interconnect available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalInterconnects(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a Interconnect in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalInterconnectsInterconnect(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, interconnect, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified interconnect.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInterconnectsInterconnect(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, interconnect, callback)</td>
    <td style="padding:15px">Returns the specified interconnect. Get a list of available interconnects by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalInterconnectsInterconnect(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, interconnect, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified interconnect with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalInterconnectsInterconnectGetDiagnostics(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, interconnect, callback)</td>
    <td style="padding:15px">Returns the interconnectDiagnostics for the specified interconnect.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/interconnects/{pathv2}/getDiagnostics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalLicenseCodesLicenseCode(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, licenseCode, callback)</td>
    <td style="padding:15px">Return a specified license code. License codes are mirrored across all projects that have permissions to read the License Code.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenseCodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalLicenseCodesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenseCodes/{pathv2}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalLicenses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of licenses available in the specified project. This method does not get any licenses that belong to other projects, including licenses attached to publicly-available images, like Debian 9. If you want to get a list of publicly-available licenses, use this method to make a request to the respective image project, such as debian-cloud or windows-cloud.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalLicenses(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Create a License resource in the specified project.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalLicensesLicense(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, license, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified license.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalLicensesLicense(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, license, callback)</td>
    <td style="padding:15px">Returns the specified License resource.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalLicensesResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses/{pathv2}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalLicensesResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses/{pathv2}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalLicensesResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.  Caution This resource is intended for use only by third-party partners who are creating Cloud Marketplace images.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/licenses/{pathv2}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of network endpoint groups that are located in the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a network endpoint group in the specified project using the parameters that are included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalNetworkEndpointGroupsNetworkEndpointGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, networkEndpointGroup, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified network endpoint group.Note that the NEG cannot be deleted if there are backend services referencing it.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalNetworkEndpointGroupsNetworkEndpointGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, networkEndpointGroup, callback)</td>
    <td style="padding:15px">Returns the specified network endpoint group. Gets a list of available network endpoint groups by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworkEndpointGroupsNetworkEndpointGroupAttachNetworkEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, networkEndpointGroup, requestId, body, callback)</td>
    <td style="padding:15px">Attach a network endpoint to the specified network endpoint group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups/{pathv2}/attachNetworkEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworkEndpointGroupsNetworkEndpointGroupDetachNetworkEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, networkEndpointGroup, requestId, body, callback)</td>
    <td style="padding:15px">Detach the network endpoint from the specified network endpoint group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups/{pathv2}/detachNetworkEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworkEndpointGroupsNetworkEndpointGroupListNetworkEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, networkEndpointGroup, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists the network endpoints in the specified network endpoint group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networkEndpointGroups/{pathv2}/listNetworkEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalNetworks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of networks available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a network in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalNetworksNetwork(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified network.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalNetworksNetwork(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, callback)</td>
    <td style="padding:15px">Returns the specified network. Gets a list of available networks by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalNetworksNetwork(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified network with the data included in the request. Only the following fields can be modified: routingConfig.routingMode.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworksNetworkAddPeering(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, requestId, body, callback)</td>
    <td style="padding:15px">Adds a peering to the specified network.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}/addPeering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalNetworksNetworkListPeeringRoutes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, direction = 'INCOMING', filter, maxResults, orderBy, pageToken, peeringName, region, callback)</td>
    <td style="padding:15px">Lists the peering routes exchanged over peering connection.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}/listPeeringRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworksNetworkRemovePeering(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, requestId, body, callback)</td>
    <td style="padding:15px">Removes a peering from the specified network.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}/removePeering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalNetworksNetworkSwitchToCustomMode(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, requestId, callback)</td>
    <td style="padding:15px">Switches the network mode from auto subnet mode to custom subnet mode.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}/switchToCustomMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalNetworksNetworkUpdatePeering(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, network, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified network peering with the data included in the request Only the following fields can be modified: NetworkPeering.export_custom_routes, and NetworkPeering.import_custom_routes</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/networks/{pathv2}/updatePeering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalRoutes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of Route resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalRoutes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a Route resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalRoutesRoute(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, route, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified Route resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalRoutesRoute(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, route, callback)</td>
    <td style="padding:15px">Returns the specified Route resource. Gets a list of available routes by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSecurityPolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">List all the policies that have been configured for the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSecurityPolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a new policy in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSecurityPoliciesListPreconfiguredExpressionSets(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Gets the current list of preconfigured Web Application Firewall (WAF) expressions.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/listPreconfiguredExpressionSets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalSecurityPoliciesSecurityPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSecurityPoliciesSecurityPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, callback)</td>
    <td style="padding:15px">List all of the ordered rules present in a single specified policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalSecurityPoliciesSecurityPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified policy with the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSecurityPoliciesSecurityPolicyAddRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, body, callback)</td>
    <td style="padding:15px">Inserts a rule into a security policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}/addRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSecurityPoliciesSecurityPolicyGetRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, priority, callback)</td>
    <td style="padding:15px">Gets a rule at the specified priority.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}/getRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSecurityPoliciesSecurityPolicyPatchRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, priority, body, callback)</td>
    <td style="padding:15px">Patches a rule at the specified priority.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}/patchRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSecurityPoliciesSecurityPolicyRemoveRule(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, securityPolicy, priority, callback)</td>
    <td style="padding:15px">Deletes a rule at the specified priority.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/securityPolicies/{pathv2}/removeRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSnapshots(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of Snapshot resources contained within the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSnapshotsResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots/{pathv2}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSnapshotsResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots/{pathv2}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSnapshotsResourceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Sets the labels on a snapshot. To learn more about labels, read the Labeling Resources documentation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots/{pathv2}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSnapshotsResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots/{pathv2}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalSnapshotsSnapshot(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, snapshot, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified Snapshot resource. Keep in mind that deleting a single snapshot might not necessarily delete all the data on that snapshot. If any data on the snapshot that is marked for deletion is needed for subsequent snapshots, the data will be moved to the next corresponding snapshot.

For more information, see Deleting snapshots.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSnapshotsSnapshot(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, snapshot, callback)</td>
    <td style="padding:15px">Returns the specified Snapshot resource. Gets a list of available snapshots by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/snapshots/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSslPolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all the SSL policies that have been configured for the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalSslPolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Returns the specified SSL policy resource. Gets a list of available SSL policies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSslPoliciesListAvailableFeatures(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all features that can be specified in the SSL policy when using custom profile.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslPolicies/listAvailableFeatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalSslPoliciesSslPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, sslPolicy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified SSL policy. The SSL policy resource can be deleted only if it is not in use by any TargetHttpsProxy or TargetSslProxy resources.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalSslPoliciesSslPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, sslPolicy, callback)</td>
    <td style="padding:15px">Lists all of the ordered rules present in a single specified policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalSslPoliciesSslPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, sslPolicy, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified SSL policy with the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/sslPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetGrpcProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists the TargetGrpcProxies for a project in the given scope.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetGrpcProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetGrpcProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetGrpcProxy in the specified project in the given scope using the parameters that are included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetGrpcProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalTargetGrpcProxiesTargetGrpcProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetGrpcProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetGrpcProxy in the given scope</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetGrpcProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetGrpcProxiesTargetGrpcProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetGrpcProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetGrpcProxy resource in the given scope.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetGrpcProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectGlobalTargetGrpcProxiesTargetGrpcProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetGrpcProxy, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified TargetGrpcProxy resource with the data included in the request. This method supports PATCH semantics and uses JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetGrpcProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetSslProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of TargetSslProxy resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetSslProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetSslProxy resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalTargetSslProxiesTargetSslProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetSslProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetSslProxy resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetSslProxiesTargetSslProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetSslProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetSslProxy resource. Gets a list of available target SSL proxies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetSslProxiesTargetSslProxySetBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetSslProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the BackendService for TargetSslProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies/{pathv2}/setBackendService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetSslProxiesTargetSslProxySetProxyHeader(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetSslProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the ProxyHeaderType for TargetSslProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies/{pathv2}/setProxyHeader?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetSslProxiesTargetSslProxySetSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetSslProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes SslCertificates for TargetSslProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies/{pathv2}/setSslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetSslProxiesTargetSslProxySetSslPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetSslProxy, requestId, body, callback)</td>
    <td style="padding:15px">Sets the SSL policy for TargetSslProxy. The SSL policy specifies the server-side support for SSL features. This affects connections between clients and the SSL proxy load balancer. They do not affect the connection between the load balancer and the backends.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetSslProxies/{pathv2}/setSslPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetTcpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of TargetTcpProxy resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetTcpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetTcpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetTcpProxy resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetTcpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectGlobalTargetTcpProxiesTargetTcpProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetTcpProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetTcpProxy resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetTcpProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGlobalTargetTcpProxiesTargetTcpProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetTcpProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetTcpProxy resource. Gets a list of available target TCP proxies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetTcpProxies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetTcpProxiesTargetTcpProxySetBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetTcpProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the BackendService for TargetTcpProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetTcpProxies/{pathv2}/setBackendService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectGlobalTargetTcpProxiesTargetTcpProxySetProxyHeader(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, targetTcpProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the ProxyHeaderType for TargetTcpProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/global/targetTcpProxies/{pathv2}/setProxyHeader?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of region resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegion(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, callback)</td>
    <td style="padding:15px">Returns the specified Region resource. Gets a list of available regions by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of autoscalers contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, autoscaler, requestId, body, callback)</td>
    <td style="padding:15px">Updates an autoscaler in the specified project using the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates an autoscaler in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectRegionsRegionAutoscalers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, autoscaler, requestId, body, callback)</td>
    <td style="padding:15px">Updates an autoscaler in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/autoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionAutoscalersAutoscaler(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, autoscaler, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified autoscaler.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/autoscalers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionAutoscalersAutoscaler(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, autoscaler, callback)</td>
    <td style="padding:15px">Returns the specified autoscaler.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/autoscalers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionBackendServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of regional BackendService resources available to the specified project in the given region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionBackendServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a regional BackendService resource in the specified project using the data included in the request. For more information, see  Backend services overview.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, backendService, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified regional BackendService resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, backendService, callback)</td>
    <td style="padding:15px">Returns the specified regional BackendService resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, backendService, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified regional BackendService resource with the data included in the request. For more information, see  Understanding backend services This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectRegionsRegionBackendServicesBackendService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, backendService, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified regional BackendService resource with the data included in the request. For more information, see  Backend services overview.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionBackendServicesBackendServiceGetHealth(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, backendService, body, callback)</td>
    <td style="padding:15px">Gets the most recent health check results for this regional BackendService.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/backendServices/{pathv3}/getHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionDiskTypes(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of regional disk types available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/diskTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionDiskTypesDiskType(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, diskType, callback)</td>
    <td style="padding:15px">Returns the specified regional disk type. Gets a list of available disk types by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/diskTypes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionDisks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of persistent disks contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, sourceImage, body, callback)</td>
    <td style="padding:15px">Creates a persistent regional disk in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionDisksDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, disk, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified regional persistent disk. Deleting a regional disk removes all the replicas of its data permanently and is irreversible. However, deleting a disk does not delete any snapshots previously made from the disk. You must separately delete snapshots.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionDisksDisk(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, disk, callback)</td>
    <td style="padding:15px">Returns a specified regional persistent disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksDiskAddResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, disk, requestId, body, callback)</td>
    <td style="padding:15px">Adds existing resource policies to a regional disk. You can only add one policy which will be applied to this disk for scheduling snapshot creation.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/addResourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksDiskCreateSnapshot(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, disk, requestId, body, callback)</td>
    <td style="padding:15px">Creates a snapshot of this regional disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/createSnapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksDiskRemoveResourcePolicies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, disk, requestId, body, callback)</td>
    <td style="padding:15px">Removes resource policies from a regional disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/removeResourcePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksDiskResize(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, disk, requestId, body, callback)</td>
    <td style="padding:15px">Resizes the specified regional persistent disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionDisksResourceGetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksResourceSetIamPolicy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksResourceSetLabels(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, requestId, body, callback)</td>
    <td style="padding:15px">Sets the labels on the target regional disk.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/setLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionDisksResourceTestIamPermissions(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, resource, body, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/disks/{pathv3}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionHealthCheckServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all the HealthCheckService resources that have been configured for the specified project in the given region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthCheckServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionHealthCheckServices(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a regional HealthCheckService resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthCheckServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionHealthCheckServicesHealthCheckService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheckService, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified regional HealthCheckService.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthCheckServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionHealthCheckServicesHealthCheckService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheckService, callback)</td>
    <td style="padding:15px">Returns the specified regional HealthCheckService resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthCheckServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionHealthCheckServicesHealthCheckService(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheckService, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified regional HealthCheckService resource with the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthCheckServices/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of HealthCheck resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionHealthChecks(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a HealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheck, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified HealthCheck resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthChecks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheck, callback)</td>
    <td style="padding:15px">Returns the specified HealthCheck resource. Gets a list of available health checks by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthChecks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HealthCheck resource in the specified project using the data included in the request. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthChecks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectRegionsRegionHealthChecksHealthCheck(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, healthCheck, requestId, body, callback)</td>
    <td style="padding:15px">Updates a HealthCheck resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/healthChecks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInstanceGroupManagers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of managed instance groups that are contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagers(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a managed instance group using the information that you specify in the request. After the group is created, instances in the group are created using the specified instance template. This operation is marked as DONE when the group is created even if the instances in the group have not yet been created. You must separately verify the status of the individual instances with the listmanagedinstances method.

A regional managed instance group can contain up to 2000 instances.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionInstanceGroupManagersInstanceGroupManager(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified managed instance group and all of the instances in that group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInstanceGroupManagersInstanceGroupManager(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, callback)</td>
    <td style="padding:15px">Returns all of the details about the specified managed instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionInstanceGroupManagersInstanceGroupManager(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Updates a managed instance group using the information that you specify in the request. This operation is marked as DONE when the group is patched even if the instances in the group are still in the process of being patched. You must separately verify the status of the individual instances with the listmanagedinstances method. This method supports PATCH semantics and uses the JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerAbandonInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Flags the specified instances to be immediately removed from the managed instance group. Abandoning an instance does not delete the instance, but it does remove the instance from any target pools that are applied by the managed instance group. This method reduces the targetSize of the managed instance group by the number of instances that you abandon. This operation is marked as DONE when the action is scheduled even if the instances have not yet been removed from the group. You must separately ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/abandonInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerApplyUpdatesToInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, body, callback)</td>
    <td style="padding:15px">Apply updates to selected instances the managed instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/applyUpdatesToInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerCreateInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Creates instances with per-instance configs in this regional managed instance group. Instances are created using the current instance template. The create instances operation is marked DONE if the createInstances request is successful. The underlying actions take additional time. You must separately verify the status of the creating or actions with the listmanagedinstances method.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/createInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerDeleteInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Flags the specified instances in the managed instance group to be immediately deleted. The instances are also removed from any target pools of which they were a member. This method reduces the targetSize of the managed instance group by the number of instances that you delete. The deleteInstances operation is marked DONE if the deleteInstances request is successful. The underlying actions take additional time. You must separately verify the status of the deleting action with the listmanagedinsta...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/deleteInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerDeletePerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, body, callback)</td>
    <td style="padding:15px">Deletes selected per-instance configs for the managed instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/deletePerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerListErrors(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all errors thrown by actions on instances for a given regional managed instance group. The filter and orderBy query parameters are not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/listErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerListManagedInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists the instances in the managed instance group and instances that are scheduled to be created. The list includes any current actions that the group has scheduled for its instances. The orderBy query parameter is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/listManagedInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerListPerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists all of the per-instance configs defined for the managed instance group. The orderBy query parameter is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/listPerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerPatchPerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Insert or patch (for the ones that already exist) per-instance configs for the managed instance group. perInstanceConfig.instance serves as a key used to distinguish whether to perform insert or patch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/patchPerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerRecreateInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Flags the specified instances in the managed instance group to be immediately recreated. The instances are deleted and recreated using the current instance template for the managed instance group. This operation is marked as DONE when the flag is set even if the instances have not yet been recreated. You must separately verify the status of the recreating action with the listmanagedinstances method.

If the group is part of a backend service that has enabled connection draining, it can take up t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/recreateInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerResize(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, size, requestId, callback)</td>
    <td style="padding:15px">Changes the intended size of the managed instance group. If you increase the size, the group creates new instances using the current instance template. If you decrease the size, the group deletes one or more instances.

The resize operation is marked DONE if the resize request is successful. The underlying actions take additional time. You must separately verify the status of the creating or deleting actions with the listmanagedinstances method.

If the group is part of a backend service that ha...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerSetInstanceTemplate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Sets the instance template to use when creating new instances or recreating instances in this group. Existing instances are not affected.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/setInstanceTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerSetTargetPools(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Modifies the target pools to which all new instances in this group are assigned. Existing instances in the group are not affected.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/setTargetPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupManagersInstanceGroupManagerUpdatePerInstanceConfigs(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroupManager, requestId, body, callback)</td>
    <td style="padding:15px">Insert or update (for the ones that already exist) per-instance configs for the managed instance group. perInstanceConfig.instance serves as a key used to distinguish whether to perform insert or patch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroupManagers/{pathv3}/updatePerInstanceConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInstanceGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of instance group resources contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionInstanceGroupsInstanceGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroup, callback)</td>
    <td style="padding:15px">Returns the specified instance group resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupsInstanceGroupListInstances(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroup, filter, maxResults, orderBy, pageToken, body, callback)</td>
    <td style="padding:15px">Lists the instances in the specified instance group and displays information about the named ports. Depending on the specified options, this method can list all instances or only the instances that are running. The orderBy query parameter is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroups/{pathv3}/listInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionInstanceGroupsInstanceGroupSetNamedPorts(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, instanceGroup, requestId, body, callback)</td>
    <td style="padding:15px">Sets the named ports for the specified regional instance group.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/instanceGroups/{pathv3}/setNamedPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of regional network endpoint groups available to the specified project in the given region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionNetworkEndpointGroups(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a network endpoint group in the specified project using the parameters that are included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/networkEndpointGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionNetworkEndpointGroupsNetworkEndpointGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, networkEndpointGroup, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified network endpoint group. Note that the NEG cannot be deleted if it is configured as a backend of a backend service.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/networkEndpointGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNetworkEndpointGroupsNetworkEndpointGroup(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, networkEndpointGroup, callback)</td>
    <td style="padding:15px">Returns the specified network endpoint group. Gets a list of available network endpoint groups by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/networkEndpointGroups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNotificationEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Lists the NotificationEndpoints for a project in the given region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/notificationEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionNotificationEndpoints(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Create a NotificationEndpoint in the specified project in the given region using the parameters that are included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/notificationEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionNotificationEndpointsNotificationEndpoint(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, notificationEndpoint, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified NotificationEndpoint in the given region</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/notificationEndpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionNotificationEndpointsNotificationEndpoint(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, notificationEndpoint, callback)</td>
    <td style="padding:15px">Returns the specified NotificationEndpoint resource in the given region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/notificationEndpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionOperations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of Operation resources contained within the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionOperationsOperation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, operation, callback)</td>
    <td style="padding:15px">Deletes the specified region-specific Operations resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/operations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionOperationsOperation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, operation, callback)</td>
    <td style="padding:15px">Retrieves the specified region-specific Operations resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/operations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionOperationsOperationWait(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, operation, callback)</td>
    <td style="padding:15px">Waits for the specified Operation resource to return as `DONE` or for the request to approach the 2 minute deadline, and retrieves the specified Operation resource. This method differs from the `GET` method in that it waits for no more than the default deadline (2 minutes) and then returns the current state of the operation, which might be `DONE` or still in progress.

This method is called on a best-effort basis. Specifically:
- In uncommon cases, when the server is overloaded, the request mi...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/operations/{pathv3}/wait?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of SslCertificate resources available to the specified project in the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/sslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a SslCertificate resource in the specified project and region using the data included in the request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/sslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionSslCertificatesSslCertificate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, sslCertificate, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified SslCertificate resource in the region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/sslCertificates/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionSslCertificatesSslCertificate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, sslCertificate, callback)</td>
    <td style="padding:15px">Returns the specified SslCertificate resource in the specified region. Get a list of available SSL certificates by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/sslCertificates/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetHttpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of TargetHttpProxy resources available to the specified project in the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetHttpProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetHttpProxy resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionTargetHttpProxiesTargetHttpProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetHttpProxy resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpProxies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetHttpProxiesTargetHttpProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetHttpProxy resource in the specified region. Gets a list of available target HTTP proxies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpProxies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetHttpProxiesTargetHttpProxySetUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the URL map for TargetHttpProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpProxies/{pathv3}/setUrlMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetHttpsProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of TargetHttpsProxy resources available to the specified project in the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpsProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetHttpsProxies(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a TargetHttpsProxy resource in the specified project and region using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpsProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionTargetHttpsProxiesTargetHttpsProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpsProxy, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified TargetHttpsProxy resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpsProxies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionTargetHttpsProxiesTargetHttpsProxy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpsProxy, callback)</td>
    <td style="padding:15px">Returns the specified TargetHttpsProxy resource in the specified region. Gets a list of available target HTTP proxies by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpsProxies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetHttpsProxiesTargetHttpsProxySetSslCertificates(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpsProxy, requestId, body, callback)</td>
    <td style="padding:15px">Replaces SslCertificates for TargetHttpsProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpsProxies/{pathv3}/setSslCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionTargetHttpsProxiesTargetHttpsProxySetUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, targetHttpsProxy, requestId, body, callback)</td>
    <td style="padding:15px">Changes the URL map for TargetHttpsProxy.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/targetHttpsProxies/{pathv3}/setUrlMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionUrlMaps(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of UrlMap resources available to the specified project in the specified region.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionUrlMaps(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, requestId, body, callback)</td>
    <td style="padding:15px">Creates a UrlMap resource in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectRegionsRegionUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, urlMap, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified UrlMap resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectRegionsRegionUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, urlMap, callback)</td>
    <td style="padding:15px">Returns the specified UrlMap resource. Gets a list of available URL maps by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectRegionsRegionUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, urlMap, requestId, body, callback)</td>
    <td style="padding:15px">Patches the specified UrlMap resource with the data included in the request. This method supports PATCH semantics and uses JSON merge patch format and processing rules.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectRegionsRegionUrlMapsUrlMap(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, urlMap, requestId, body, callback)</td>
    <td style="padding:15px">Updates the specified UrlMap resource with the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectRegionsRegionUrlMapsUrlMapValidate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, region, urlMap, body, callback)</td>
    <td style="padding:15px">Runs static validation for the UrlMap. In particular, the tests of the provided UrlMap will be run. Calling this method does NOT create the UrlMap.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/regions/{pathv2}/urlMaps/{pathv3}/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZones(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves the list of Zone resources available to the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZone(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, callback)</td>
    <td style="padding:15px">Returns the specified Zone resource. Gets a list of available zones by making a list() request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneOperations(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, filter, maxResults, orderBy, pageToken, callback)</td>
    <td style="padding:15px">Retrieves a list of Operation resources contained within the specified zone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectZonesZoneOperationsOperation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, operation, callback)</td>
    <td style="padding:15px">Deletes the specified zone-specific Operations resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/operations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectZonesZoneOperationsOperation(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, operation, callback)</td>
    <td style="padding:15px">Retrieves the specified zone-specific Operations resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/operations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectZonesZoneOperationsOperationWait(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, project, zone, operation, callback)</td>
    <td style="padding:15px">Waits for the specified Operation resource to return as `DONE` or for the request to approach the 2 minute deadline, and retrieves the specified Operation resource. This method differs from the `GET` method in that it waits for no more than the default deadline (2 minutes) and then returns the current state of the operation, which might be `DONE` or still in progress.

This method is called on a best-effort basis. Specifically:
- In uncommon cases, when the server is overloaded, the request mi...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/zones/{pathv2}/operations/{pathv3}/wait?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFirewallPolicyAssociation(resourceId, association, requestId, replaceExistingAssociation, callback)</td>
    <td style="padding:15px">Inserts an association for the specified firewall policy</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/addAssociation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFirewallPolicyRule(resourceId, rule, requestId, callback)</td>
    <td style="padding:15px">Inserts a rule into a firewall policy</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/addRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneFirewallPolicyRule(resourceId, requestId, sourceFirewallPolicy, callback)</td>
    <td style="padding:15px">Copies rules to the specified firewall policy</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/cloneRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallPolicy(resourceId, requestId, callback)</td>
    <td style="padding:15px">Deletes the specified policy.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallPolicy(resourceId, callback)</td>
    <td style="padding:15px">Returns the specified firewall policy</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallPolicyAssociation(resourceId, name, callback)</td>
    <td style="padding:15px">Gets an association with the specified name.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/getAssociation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallPolicyIamPolicy(resourceId, optionsRequestedPolicyVersion, callback)</td>
    <td style="padding:15px">Gets the access control policy for a resource. May be empty if no such policy or resource exists.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/getIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallPolicyRule(resourceId, priority, callback)</td>
    <td style="padding:15px">Gets a rule of the specified priority.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/getRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertFirewallPolicy(policy, requestId, parentId, callback)</td>
    <td style="padding:15px">Creates a new policy in the specified project using the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/firewallPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallPolicy(maxResults, pageToken, filter, orderBy, returnPartialSuccess, parentId, callback)</td>
    <td style="padding:15px">Lists all the policies that have been configured for the specified folder or organization</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/firewallPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallPolicyAssociations(targetResource, callback)</td>
    <td style="padding:15px">Lists associations of a specified target, i.e., organization or folder.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/firewallPolicies/listAssociations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveFirewallPolicy(resourceId, requestId, parentId, callback)</td>
    <td style="padding:15px">Moves the specified firewall policy</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFirewallPolicy(resourceId, policy, requestId, callback)</td>
    <td style="padding:15px">Patches the specified policy with the data included in the request.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFirewallPolicyRule(resourceId, rule, requestId, priority, callback)</td>
    <td style="padding:15px">Patches a rule of the specified priority.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/patchRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeFirewallPolicyAssociation(resourceId, requestId, name, callback)</td>
    <td style="padding:15px">Removes an association for the specified firewall policy.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/removeAssociation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeFirewallPolicyRule(resourceId, requestId, priority, callback)</td>
    <td style="padding:15px">Deletes a rule of the specified priority</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/removeRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setFirewallPolicyIamPolicy(resourceId, iamPolicy, callback)</td>
    <td style="padding:15px">Sets the access control policy on the specified resource. Replaces any existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/setIamPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testFirewallPolicyIamPermissions(resourceId, iamPolicy, callback)</td>
    <td style="padding:15px">Returns permissions that a caller has on the specified resource</td>
    <td style="padding:15px">{base_path}/{version}/locations/global/{pathv1}/testIamPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
