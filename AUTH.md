## Authenticating Google Cloud Platform Compute Adapter 

This document will go through the steps for authenticating the Google Cloud Platform Compute adapter with Google Token Security. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>.

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

### Google Token Authentication
The Google Cloud Platform Compute adapter authenticates with a Google token. 

STEPS
1. Ensure you have access to a Google Cloud Platform Compute server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field

#### Using key in service config (Needed for Cloud Instance)
```json
"authentication": {
  "auth_method": "no_authentication",
  "configureGtoken": {
    "email": "sample@email.com",
    "key": "-----BEGIN PRIVATE KEY-----\nMIIEv********--\n",
    "scope": "https://www.********",
    "eagerRefreshThresholdMillis": 300000,
    "project_id": "myprojectid"
  }
}
```

#### Using keyfile in service config 
```json
"authentication": {
  "auth_method": "no_authentication",
  "configureGtoken": {
    "email": "",
    "scope": "https://www.googleapis.com/auth/cloud-platform",
    "sub": "",
    "keyFile": "/opt/pronghorn/RENAME_TO_YOUR_KEYFILE.json",
    "key": "",
    "additionalClaims": "",
    "eagerRefreshThresholdMillis": 300000
  }
}
```

you can leave all of the other properties in the authentication section, they will not be used for Google Cloud Platform Compute Google token authentication.
4. Restart the adapter. If your properties were set correctly, the adapter should go online.

### Troubleshooting
- Make sure you copied over the correct Googgle properties as these are used to retrieve the token.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
